<?php
/**
 * @category   aac
 */


namespace Aac;

use PHPUnit_Framework_TestCase;


/**
 * Class AacTest
 * @package Aac
 */
class AacTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function userHasAccessToAGroupHeIsIn()
    {
        $aac = new Aac();

        $group = new Group(1);

        $aac->addGroup($group);

        $aac->userToGroup(1000, 1);

        $this->assertTrue($aac->hasAccess(1000, 1));
    }

    /**
     * @test
     */
    public function userHasAccessToAParentGroupHeIsIn()
    {
        $aac = new Aac();

        $aac->addGroup(new Group(1));
        $aac->addGroup(new Group(2, [1]));

        $aac->userToGroup(1000, 2);

        $this->assertTrue($aac->hasAccess(1000, 1));
    }

    /**
     * @test
     */
    public function userHasAccessToAParentGroupHeIsIn2()
    {
        $aac = new Aac();

        $aac->addGroup(new Group(1));
        $aac->addGroup(new Group(2, [1]));
        $aac->addGroup(new Group(3, [2]));
        $aac->addGroup(new Group(4, [3]));

        $aac->userToGroup(1000, 4);

        $this->assertTrue($aac->hasAccess(1000, 2));
    }

    /**
     * @test
     */
    public function recursion()
    {
        $aac = new Aac();

        $aac->addGroup(new Group(1, [4]));
        $aac->addGroup(new Group(2, [1]));
        $aac->addGroup(new Group(3, [2]));
        $aac->addGroup(new Group(4, [3]));

        $aac->userToGroup(1000, 4);

        $this->assertTrue($aac->hasAccess(1000, 2));
    }
}

