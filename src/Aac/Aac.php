<?php
/**
 * @category   aac
 */


namespace Aac;


/**
 * Class Aac
 * @package Aac
 */
class Aac
{
    /**
     * @var Group[]
     */
    private $groups = [];

    /**
     * @var array
     */
    private $existingGroups = [];

    /**
     * @var array
     */
    private $userGroupMapping = [];

    /**
     * @var array
     */
    private $alreadyCheckedGroupIds = [];

    /**
     * @var array
     */
    private $parentChildGroupRelations = [];

    /**
     * @param Group $group
     */
    public function addGroup(Group $group)
    {
        $groupId                        = $group->getId();
        $this->groups[$groupId]         = $group;
        $this->existingGroups[$groupId] = true;

        foreach ($group->getParentGroupIds() as $parentGroupId) {
            $this->parentChildGroupRelations[] = [
                'parentGroupId' => $parentGroupId,
                'childGroupId'  => $groupId,
            ];
        }
    }

    /**
     * @param int $userId
     * @param int $groupId
     *
     * @return bool
     */
    public function userToGroup($userId, $groupId)
    {
        $userId  = (int) $userId;
        $groupId = (int) $groupId;

        if (!isset($this->existingGroups[$groupId])) {
            return false;
        }

        $this->userGroupMapping[] = [
            'userId'  => $userId,
            'groupId' => $groupId
        ];

        return true;
    }

    /**
     * @param $userId
     * @param $groupId
     *
     * @return bool
     */
    public function hasAccess($userId, $groupId)
    {
        $this->alreadyCheckedGroupIds = [];

        return $this->hasAccessRecursive($userId, $groupId);
    }

    /**
     * @param $userId
     * @param $groupId
     *
     * @return bool
     */
    private function hasAccessRecursive($userId, $groupId)
    {
        $userId  = (int) $userId;
        $groupId = (int) $groupId;

        foreach ($this->userGroupMapping as $userGroupMapping) {
            if ($userGroupMapping['groupId'] === $groupId && $userGroupMapping['userId'] === $userId) {
                return true;
            }
        }

        foreach ($this->userGroupMapping as $userGroupMapping) {
            if ($userGroupMapping['userId'] === $userId) {
                $childGroupIds = $this->getChildGroupIds($userGroupMapping['groupId']);

                foreach ($childGroupIds as $childGroupId) {

                    if (isset($this->alreadyCheckedGroupIds[$childGroupId])) {
                        continue;
                    } else {
                        $this->alreadyCheckedGroupIds[$childGroupId] = true;
                    }

                    $hasAccessViaParentGroup = $this->hasAccessRecursive($userId, $childGroupId);

                    if ($hasAccessViaParentGroup) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param $groupId
     *
     * @return Group
     */
    private function getGroupById($groupId)
    {
        return $this->groups[$groupId];
    }

    /**
     * @param $groupId
     *
     * @return array
     */
    private function getChildGroupIds($groupId)
    {
        $childGroupIds = [];
        foreach ($this->parentChildGroupRelations as $parentChildGroupRelation) {
            if ($parentChildGroupRelation['parentGroupId']) {
                $childGroupIds[] = $parentChildGroupRelation['childGroupId'];
            }
        }

        return $childGroupIds;
    }
}