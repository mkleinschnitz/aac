<?php
/**
 * @category   aac
 */


namespace Aac;


/**
 * Class Group
 * @package Aac
 */
class Group
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $parentGroupIds;

    /**
     * @param       $id
     * @param array $parentGroupIds
     */
    public function __construct($id, array $parentGroupIds = [])
    {
        $this->id             = $id;
        $this->parentGroupIds = $parentGroupIds;
    }

    /**
     * @return array
     */
    public function getParentGroupIds()
    {
        return $this->parentGroupIds;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
} 